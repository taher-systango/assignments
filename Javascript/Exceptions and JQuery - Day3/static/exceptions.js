"use strict";

//RangeError
const num = 12.232;
try {
    document.write(num.toFixed(200));
} catch (error) {
    if (error instanceof RangeError) {
        document.write('Range error occured</br>');
    }
}


//ReferenceError
try {
    document.write(abc);
} catch (error) {
    if (error instanceof ReferenceError) {
        document.write('abc variable is not defined</br>');
    }
}


//SyntaxError
// try {
//     document.write(abc);


//TypeError
try {
    num.toUpperCase();
}
catch (error) {
    if (error instanceof TypeError)
        document.write('Variable num is a number not a string.');
}


//URIError
try {
    decodeURIComponent('%');
} catch (error) {
    if (error instanceof URIError) {
        document.write('URL is invalid</br>');
    }
}


//CustomError
function myError(message) {
    this.name = "CustomError";
    this.message = message;
}

function addPositive(a, b) {
    if (a < 0 || b < 0) {
        throw new myError('Only positive numbers are alllowed.');
    } else {
        return a + b;
    }
}

try {
    addPositive(-1, 2);
} catch (error) {
    if (error instanceof myError) {
        document.write(error.name + '</br>');
        document.write(error.message + '</br>');
    }
}