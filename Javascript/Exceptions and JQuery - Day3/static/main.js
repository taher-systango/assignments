$(document).ready(function () {
    $('#addBtn').click(() => {
        $('#myForm').prepend('<div class="textDiv"> <input class="textInput" type="text"></input><input type="button" class="showBtn btn btn-success" value="Show Value"></input><input type="button" class="removeBtn btn btn-danger" value="Remove"></input></div>')
    });

    $(document).on('click', '.showBtn', function () {
        const textBox = $(this).prev();
        if ($(textBox).siblings('span').length == 0) {
            //Show
            const text = textBox.val();
            if (text.length > 0) {
                $("</br><span>" + text + "</span>").insertAfter($(textBox).nextAll().last());
                $(this).val('Hide Value')
            } else {
                alert('Enter some text')
            }
        } else {
            //Hide
            $(this).val('Show Value')
            $(textBox).siblings('span').remove()
            $(textBox).siblings('br').remove()
        }
    });

    $(document).on('click', '.removeBtn ', function () {
        $(this).parent().remove()
    });

    $('#changeColorBtn').click(function () {
        numOfTextDivs = $('.textDiv').length;
        if (numOfTextDivs > 0) {
            const randomColorPos = Math.floor(Math.random() * $('.textDiv').length + 1);
            const randomColor = "#" + ((1 << 24) * Math.random() | 0).toString(16);
            $('.textDiv:nth-child(' + randomColorPos + ')').children().first().css("background-color", randomColor);
        } else {
            alert('No text boxes found')
        }
    });
});