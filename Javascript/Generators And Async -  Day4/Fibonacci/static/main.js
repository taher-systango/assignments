function* fibonacci(n) {
    let current = 0;
    let previous = 1;
    yield current;
    while (current + previous < n) {
        yield current + previous;
        [previous, current] = [current, current + previous];
    }
}

var f = fibonacci(212);

for (const number of f) {
    document.write(number + '</br>');
}