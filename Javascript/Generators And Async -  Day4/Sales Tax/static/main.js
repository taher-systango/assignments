const products = [
    {
        name: "A4 Sheets 100pc",
        price: 100,
        country: "IN"
    }, {
        name: "A3 Sheets 100pc",
        price: 160,
        country: "USA"
    }, {
        name: "A4 Sheets 200pc",
        price: 150,
        country: "UK"
    }, {
        name: "A5 Sheets 100pc",
        price: 50,
        country: "AUS"
    }
];

function calculate_tax(products) {
    const calculated_products = [];
    products.forEach(product => {
        calculated_products.push(apply_tax(product));
    });

    //Printing out the calculated products which include the final price with tax rate applied.
    console.log(calculated_products);
}

function apply_tax(product) {
    switch (product.country) {
        case "IN":
            product.final_price = (product.price * 0.05) + product.price
            break;
        case "USA":
            product.final_price = (product.price * 0.10) + product.price
            break;
        case "AUS":
            product.final_price = (product.price * 0.13) + product.price
            break;
        case "UK":
            product.final_price = (product.price * 0.7) + product.price
            break;
        default:
            product.final_price = product.price
            break;
    }
    return product;
}

calculate_tax(products);