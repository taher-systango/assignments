"use strict";

const names = ["John", "Peter", "Alex", "Micheal"];
let counter = 0;
//Starting 3 seconds interval call to display_name and 
//getting interval_id for stopping the interval loop once all the names are displayed. 
var interval_id = setInterval(display_name, 3000);

function display_name() {
    if (counter == names.length - 1) {
        //All the names are displayed, stopping the loop
        clearInterval(interval_id);
    }
    document.write(names[counter] + '</br>');
    counter++;
}
