"use strict";

//Creating a array or products, using different date constructors
const product_list = [{
    id: 12,
    name: "Kitkat",
    manu_date: new Date("25 Jan 2019 11:30:00"),
    expire_date: new Date("25 July 2020 11:30:00"),
    is_available: true
}, {
    id: 34,
    name: "Lays",
    manu_date: new Date(1517223872003),
    expire_date: new Date(1548760265000),
    is_available: false
}, {
    id: 1,
    name: "Balaji",
    manu_date: new Date(2019, 4, 23, 12, 11, 23, 232),
    expire_date: new Date(2020, 0, 23, 12, 11, 23, 232),
    is_available: false
}, {
    id: 23,
    name: "Dairy Milk",
    manu_date: new Date("14 July 2020 12:10:03"),
    expire_date: new Date("14 July 2021 12:10:03"),
    is_available: true
},]

//Sorting products into acending order and displaying it on the document.
const sorted_product_list = sort_products(product_list);
display_products(sorted_product_list, "Sorted product ids");

//Asking for search term from the user.
const filter_term = prompt("Enter search term").toLocaleLowerCase();
//Searching products and displaying it on the document.
const filtered_products = filter_products(product_list, filter_term)
display_products(filtered_products, "Search results");

//Filtering unavailable products and displaying it on the document.
const available_products = filter_unavailable(product_list)
display_products(available_products, "Products available")

//Removes all the products which are not available
function filter_unavailable(product_list) {
    return product_list.filter((product) => {
        return product.is_available;
    });
}

//Used for search functionality
function filter_products(product_list, filter_term) {
    return product_list.filter(product =>
        product.name.toLocaleLowerCase().includes(filter_term));
}

//Sorting product_list in acending order based on their ID
function sort_products(product_list) {
    product_list.sort((product_a, product_b) => {
        return (product_a.id > product_b.id) ? 1 : -1;
    })
    return product_list;
}

//Function to display the products on the document..
function display_products(product_list, heading) {
    document.write("<b>" + heading + "</b></br>");
    if (product_list.length == 0) {
        document.write('No products </br>')
    } else {
        product_list.forEach(product => {
            document.write('<li>' + product.id + " - " + product.name + "</li>");
        });
    }
    document.write("<hr>");
}