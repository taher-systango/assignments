from enum import Enum


class Direction(Enum):
    NORTH = 1
    SOUTH = 2
    EAST = 3
    WEST = 4

    @staticmethod
    def get_move_operation_from_char(char):
        if char == 'S':
            return [0, -1]
        elif char == 'N':
            return [0, 1]
        elif char == 'E':
            return [1, 0]
        elif char == 'W':
            return [-1, 0]


class MirrorDirection(Enum):
    FORWARD = 1
    BACKWARD = 2

    @staticmethod
    def get_direction_from_char(char):
        if char == '/':
            return MirrorDirection.FORWARD
        elif char == '\\':
            return MirrorDirection.BACKWARD
