from builtins import int

from utils import Direction, MirrorDirection
import sys


class Laser:
    def __init__(self, x: int, y: int, direction: []):
        self.x = x
        self.y = y
        self.direction = direction


class Mirror:
    def __init__(self, x: int, y: int, direction: MirrorDirection):
        self.x = x
        self.y = y
        self.direction = direction


class Game:

    def __init__(self, width: int, height: int, laser: Laser, mirror_list: [Mirror]):
        self.width = width
        self.height = height
        self.moves = []
        self.laser = laser
        self.mirror_list = mirror_list
        self.in_loop = False

    def run(self):
        while game.next_move():
            print('-' * 10)

    def next_move(self):
        self.move_laser()

        if self.check_collision():
            print('Laser collided at', self.laser.x, self.laser.y)
            return False

        if self.has_been_before():
            self.in_loop = True
            print('Laser is in a loop', self.laser.x, self.laser.y)
            return False

        self.check_for_mirror()

        print('Moved Laser at', self.laser.x, self.laser.y, 'incrementing [x,y]', self.laser.direction)
        self.moves.append((self.laser.x, self.laser.y))
        return True

    def has_been_before(self):
        for move in self.moves:
            if self.laser.x == move[0] and self.laser.y == move[1]:
                return True
        return False

    def move_laser(self):
        # Moving laser one step
        self.laser.x += self.laser.direction[0]
        self.laser.y += self.laser.direction[1]

    def check_collision(self):
        if self.laser.x > self.width:
            return True
        elif self.laser.x < 0:
            return True
        elif self.laser.y > self.height:
            return True
        elif self.laser.y < 0:
            return True
        else:
            return False

    def check_for_mirror(self):
        for mirror in self.mirror_list:
            if mirror.x == self.laser.x and mirror.y == self.laser.y:
                print('Matched a mirror at', self.laser.x, self.laser.y, mirror.direction)
                if mirror.direction == MirrorDirection.BACKWARD:
                    self.laser.direction[0], self.laser.direction[1] = -(self.laser.direction[1]), -(
                        self.laser.direction[0])
                elif mirror.direction == MirrorDirection.FORWARD:
                    self.laser.direction[0], self.laser.direction[1] = self.laser.direction[1], self.laser.direction[0]

                print('changed laser increment to [x,y]', self.laser.direction)


# Init
input_filename = sys.argv[1]
output_filename = sys.argv[2]

with open(input_filename) as file:
    input_data = file.read().split('\n')

width, height = input_data[0].split(' ')
laser_x, laser_y, laser_direction_char = input_data[1].split(' ')
laser_direction = Direction.get_move_operation_from_char(laser_direction_char)
my_mirror_list = list()

for row in input_data[2::]:
    if len(row) > 0:
        x, y, mirror_direction_char = row.split()
        my_mirror_list.append(Mirror(int(x), int(y), MirrorDirection.get_direction_from_char(mirror_direction_char)))

my_laser = Laser(int(laser_x), int(laser_y), laser_direction)
game = Game(int(width), int(height), my_laser, my_mirror_list)

game.run()

print('Total moves', len(game.moves))
print('All Moves', game.moves)
print('Last position', game.moves[-1])

if game.in_loop:
    text_to_write = "-1"
else:
    text_to_write = f"{len(game.moves)}\n{game.moves[-1][0]} {game.moves[-1][1]}"

with open(output_filename, 'w')as file:
    file.write(text_to_write)
