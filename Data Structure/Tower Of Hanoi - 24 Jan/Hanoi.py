def calculate_moves(no_of_discs, A, C, B):
    '''
    Function to calculate the required moves to solve the Tower of Hanoi problem using recursion.
    We are moving discs from A to C using B.
    '''
    if no_of_discs == 0:
        return
    # Move n-1 discs from A to B
    calculate_moves(no_of_discs-1, A, B, C)
    # Move the bottom-most disc from A to C
    print('Moving from', A, 'to', C)
    # Move the discs present in B (moved in first step) to C
    calculate_moves(no_of_discs-1, B, C, A)


calculate_moves(4, 'A', 'C', 'B')
