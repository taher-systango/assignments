import java.util.HashMap;
import java.util.Map;

public class Hash {
    public static void main(String args[]) {
        String[] names = { "john", "Johnny", "john", "Tom", "Harry", "Harry" };
        HashMap<String, Integer> personCountMap = new HashMap<>();

        // Calculating the occurances of the elements in the names array.
        for (String name : names) {
            personCountMap.put(name, personCountMap.getOrDefault(name, 0) + 1);
        }
        // Printing out the occurance count of each element.
        for (Map.Entry<String, Integer> personCountEntrySet : personCountMap.entrySet()) {
            System.out.println("Element " + personCountEntrySet.getKey() + " occurs " + personCountEntrySet.getValue()
                    + " times.");
        }
        // Printing out all the keys present in the hashmap.
        System.out.println("Keys : " + personCountMap.keySet());
    }
}